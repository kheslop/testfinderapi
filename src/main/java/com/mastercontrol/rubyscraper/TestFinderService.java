package com.mastercontrol.rubyscraper;

import com.mastercontrol.rubyscraper.config.RubyScraperConfig;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;


@Service
public class TestFinderService {

    RubyScraper rubyScraper = new RubyScraper();
    RubyScraperConfig rubyScraperConfig = new RubyScraperConfig();
    TestData testData = new TestData();

    public List<String> scrapeTests(String key, String secondKey, boolean validation, boolean functional, boolean testPaths, boolean andOrToggle) {
        List<TestData> allResults = new ArrayList<>();
        if(andOrToggle) {
            if (validation) {
                allResults.addAll(getMatchingTestsUsingKeywordAndKeyword(rubyScraperConfig.getPathToValidationFRS(), key, secondKey));
            }
            if (functional) {
                allResults.addAll(getMatchingTestsUsingKeywordAndKeyword(rubyScraperConfig.getPathToFunctionalTests(), key, secondKey));
            }
        } else {
            if (validation) {
                allResults.addAll(getMatchingTestsUsingKeywordOrKeyword(rubyScraperConfig.getPathToValidationFRS(), key, secondKey));
            }
            if (functional) {
                allResults.addAll(getMatchingTestsUsingKeywordOrKeyword(rubyScraperConfig.getPathToFunctionalTests(), key, secondKey));
            }
        }
        if(testPaths) {
            return removeDuplicateResultsFromList(rubyScraper.getFilePathOfMatchingTests(allResults));
        }
        return removeDuplicateResultsFromList(testData.getTestNamesAsStringList(allResults));
    }

    public List<TestData> getMatchingTestsUsingKeywordAndKeyword(String pathToTests, String key, String secondKey) {
        List<TestData> scrapedData = rubyScraper.scraper(new File(pathToTests));
        List<TestData> searchResults = getListUsingKeywordSearchWithOptionalAnd(scrapedData, key.toLowerCase(), secondKey.toLowerCase());
        return searchResults;
    }

    public List<TestData> getMatchingTestsUsingKeywordOrKeyword(String pathToTests, String key, String secondKey) {
        List<TestData> scrapedData = rubyScraper.scraper(new File(pathToTests));
        List<TestData> searchResults = getListUsingKeywordSearchWithOptionalOr(scrapedData, key.toLowerCase(), secondKey.toLowerCase());
        return searchResults;
    }

    public List<TestData> getListUsingKeywordSearchWithOptionalAnd(List<TestData> scrapedData, String key, String secondKey) {
        List<TestData> keywordSearchResults = new ArrayList<>();
        for(TestData dataList : scrapedData) {
            if(secondKey.isEmpty() && dataList.testData.contains(key)
                || dataList.testData.contains(key) && dataList.testData.contains(secondKey)) {
                keywordSearchResults.add(dataList);
            }
        }
        return keywordSearchResults;
    }

    public List<TestData> getListUsingKeywordSearchWithOptionalOr(List<TestData> scrapedData, String key, String secondKey) {
        List<TestData> keywordSearchResults = new ArrayList<>();
        for(TestData dataList : scrapedData) {
            if(dataList.testData.contains(key) || dataList.testData.contains(secondKey)) {
                keywordSearchResults.add(dataList);
            }
        }
        return keywordSearchResults;
    }

    private List<String> removeDuplicateResultsFromList(List<String> scraperResults) {
        Set set = new TreeSet(scraperResults);
        return new ArrayList<>(set);
    }
}
