package com.mastercontrol.rubyscraper;

import com.mastercontrol.rubyscraper.config.RubyScraperConfig;
import com.mastercontrol.rubyscraper.utils.LocalFileUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RubyScraper {

    RubyScraperConfig rubyScraperConfig = new RubyScraperConfig();
    LocalFileUtils localFileUtils = new LocalFileUtils();

    public List<String> getFilePathOfMatchingTests(List<TestData> searchResults) {
        List<String> testPathsAsString = new ArrayList<>();
        List<File> testFilePaths = new ArrayList<>();
        testFilePaths.addAll(getFilePath(new File((rubyScraperConfig.getPathToValidationFRS()))));
        testFilePaths.addAll(getFilePath(new File((rubyScraperConfig.getPathToFunctionalTests()))));
        testPathsAsString.addAll(getFilePathsFromSearchResults(testFilePaths, searchResults));
        return testPathsAsString;
    }

    public List<String> getFilePathsFromSearchResults(List<File> filePaths, List<TestData> searchResults) {
        List<String> testPaths = new ArrayList<>();
        for(TestData data : searchResults) {
            for(File file : filePaths) {
                if(file.toString().contains((data.testName))) {
                    testPaths.add(String.valueOf(file));
                }
            }
        }
        List<String> strippedPaths = localFileUtils.stripAndBuildTestPath(testPaths);
        return strippedPaths;
    }

    public List<TestData> scraper(File path) {
        return localFileUtils.getFilesFromDirectory(localFileUtils.getDirectories(path))
                .stream()
                .map(file -> readFileToString(new File(String.valueOf(file))))
                .collect(Collectors.toList());
    }

    public List<File> getFilePath(File path) {
        List<File> parsedData = new ArrayList<>();
        List<File> files = localFileUtils.getFilesFromDirectory(localFileUtils.getDirectories(path));
            for(File file : files) {
                parsedData.add(getRubyFilePaths(file));
            }
            return parsedData;
        }

    public TestData readFileToString(File rubyFile) {
        TestData data = new TestData();
        data.testName = rubyFile.getName();
        try {
            data.testData = FileUtils.readFileToString(rubyFile, "UTF-8");
            return data;
        } catch (IOException e) {
            e.printStackTrace();
            return data;
        }
    }

    public File getRubyFilePaths(File rubyFile) {
        return new File(rubyFile.getAbsolutePath());
    }
}
