package com.mastercontrol.rubyscraper.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LocalFileUtils {

    public List<File> getFilesFromDirectory(List<File> files){
        List<File> returnFiles = new ArrayList<>();
        if(files != null && files.size() > 0) {
            for(File file : files) {
                if(file.isDirectory()) {
                    returnFiles.addAll(getFilesFromDirectory(Arrays.asList(file.listFiles())));
                } else {
                    returnFiles.add(file);
                }
            }
            return returnFiles;
        }
        return returnFiles;
    }

    public List<File> getDirectories(File initialDirectoryPath) {
        List<File> isDirectory = new ArrayList<>();
        for(File file : Arrays.asList(initialDirectoryPath.listFiles())) {
            if(file.isDirectory()) {
                isDirectory.add(file);
            }
        }
        return isDirectory;
    }

    public List<String> stripAndBuildTestPath(List<String> results) {
        List<String> stripList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for (String result : results) {
            sb.append(result.replace("\\", "/"));
            sb.delete(0, 19);
            sb.insert(0, "\"");
            sb.insert(sb.length(), "\"");
            stripList.add(sb.toString());
            sb.setLength(0);
        }
        return stripList;
    }

}
